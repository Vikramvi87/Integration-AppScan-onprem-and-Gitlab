write-host "======== Step: Checking Security Gate ========"
# Loading ozasmt (AppScan Source result scan file) file into a variable
#[XML]$xml=Get-Content *.ozasmt -Raw
Get-ChildItem *.ozasmt | ForEach-Object {
    $xmlContent = Get-Content $_.FullName -Raw
    $xml = [xml]$xmlContent  # Parse the content into an XML object

    [int]$highIssues = $xml.AssessmentRun.AssessmentStats.total_high_finding
    [int]$mediumIssues = $xml.AssessmentRun.AssessmentStats.total_med_finding
    [int]$lowIssues = $xml.AssessmentRun.AssessmentStats.total_low_finding
    [int]$totalIssues = $highIssues+$mediumIssues+$lowIssues

    # Process $totalIssues here...
}
$LowMaxIssuesAllowed = $LowMaxIssuesAllowed -as [int]
$HighMaxIssuesAllowed = $HighMaxIssuesAllowed -as [int]
$MedMaxIssuesAllowed = $MedMaxIssuesAllowed -as [int]
write-host "There is $highIssues high issues, $mediumIssues medium issues and $lowIssues low issues."
# write-host "The company policy permit less than $maxIssuesAllowed $sevSecGw severity."
 
if (( $highIssues -gt $HighMaxIssuesAllowed ) -and ( "$HighSevSecGw" -eq "highIssues" )) {
  write-host "Security Gate build failed";
  write-host "The company policy permit less than $HighMaxIssuesAllowed $HighSevSecGw severity.";
  exit 1
  }
elseif (( $mediumIssues -gt $MedMaxIssuesAllowed ) -and ( "$MedSevSecGw" -eq "mediumIssues" )) {
  write-host "Security Gate build failed";
  write-host "The company policy permit less than $MedMaxIssuesAllowed $MedSevSecGw severity.";
  exit 1
  }
elseif (( $lowIssues -gt $LowMaxIssuesAllowed ) -and ( "$LowSevSecGw" -eq "lowIssues" )) {
  write-host "Security Gate build failed";
  write-host "The company policy permit less than $LowMaxIssuesAllowed $LowSevSecGw severity.";
  exit 1
  }
else{
write-host "Security Gate passed"
  }
# If you want to delete every files after execution
#Remove-Item -path $CI_PROJECT_DIR\* -recurse -exclude *.pdf,*.json,*.xml